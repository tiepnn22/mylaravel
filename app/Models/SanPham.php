<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SanPham extends Model
{
    use HasFactory;

    // Kết nối model với bảng trong csdl
    protected $table = "sanpham";
    public $timestamps = false;

    public function loaisanpham() {
        // hasOne()             Một - Một, Liên kết từ bảng cha tới bảng con
        // belongsTo()          Một - Một, Liên kết từ bảng con tới bảng cha
        // hasMany()            Một - Nhiều
        // belongsToMany()      Nhiều - Nhiều
        // hasManyThrough()     Liên kết qua bảng trung gian
        
        // Liên kết khoá id của bảng loaisanpham, với khoá id_loaisanpham của bảng sanpham, và trẩ về thông tin loaisanpham
        return $this->belongsTo('App\Models\LoaiSanPham', 'id_loaisanpham', 'id');
    }
}
