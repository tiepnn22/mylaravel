<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
// use App\Http\Requests;

class MyController extends Controller
{
    // Controller
    public function XinChao() {
        echo "Chào các bạn !";
    }

    // Truyền tham số cho Controller
    public function KhoaHoc($ten) {
        echo "Khoá học : ".$ten;

        // return redirect()->route('MyRoute1');   Gọi Route trong Controller (bằng định danh)
    }

    // URL trên Request
    public function GetURL(Request $request) {
        if( $request->isMethod('get') )
            echo "Phương thức Get";
        else
            echo "Ko phải phương thức Get";

        // return $request->path();             Hiển thị path của route gọi hàm này lên
        // return $request->url();              Hiển thị url đầy đủ của route gọi hàm này lên
        // if( $request->is('admin*') );        Kiểm tra url có chứa chuỗi admin hay ko
        // if( $request->isMethod('get') );     Kiểm tra phương thức truyền là gì
    }

    // Gửi nhận dữ liệu trên Request với Form
    public function postForm(Request $request) {
        echo "Họ tên : ".$request->HoTen;

        if( $request->has('HoTen') )
            echo "Có tham số";
        else
            echo "Ko có tham số";

        // $request->has('name')                Kiểm tra tham số name có tồn tại ko
        // $request->input('id')                Nhận dữ liệu từ thẻ <input name='id'>
        // $request->input('products.0.name')   Nhận dữ liệu từ mảng
        // $request->input('user.name')         Nhận dữ liệu từ json dạng mảng
        // $request->all()                      Nhận hết dữ liệu lưu thành dạng mảng
        // $request->only('age')                Chỉ nhận tham số age
        // $request->except('age')              Nhận tất cả tham số ngoại trừ tham số age
    }

    // Cookie
    public function setCookie() {
        $response = new Response();
        $response->withCookie('KhoaHoc', 'Laravel - Khoa Pham', 1);     // 1 phút
        echo "Đã set Cookie";

        return $response;
    }

    public function getCookie(Request $request) {
        echo "Cookie của bạn là : ";

        return $request->cookie('KhoaHoc');
    }

    // Upload file
    public function postFile(Request $request) {
        if( $request->hasFile('myFile') ) {
            $file = $request->file('myFile');   // Lấy file

            if( $file->getClientOriginalExtension('myFile') == "jpg" ) {  // Check định dạng
                $filename = $file->getClientOriginalName('myFile');     // Lấy tên file
                $file->move('img', $filename);                          // Lưu file vào folder public/img
                echo "Đã lưu file :".$filename;
            } else {
                echo "Chỉ được upload file định dạng jpg";
            }
        } else {
            echo "Chưa có file";
        }

        // getClientSize('myFile')                 Trả về dung lượng file, tính theo bytes
        // getClientMimeType('myFile')             Trả về kiểu của file : image/png
        // getClientOriginalName('myFile')         Trả về tên của file
        // getClientOriginalExtension('myFile')    Trả về đuôi của file : jpg/png
        // isValid('myFile')                       Kiểm tra upload file có thành công hay không
    }

    // Xuất dữ liệu dạng JSON
    public function getJson() {
        $array = [
            'KhoaHoc1' => 'Laravel',
            'KhoaHoc2' => 'Php'
         ];

        return response()->json($array);
    }

    // View
    // Kết nối sang file View
    public function myView() {
        return view('view.KhoaPham');
    }
    // Truyền tham số sang View
    public function Time($t) {
        return view('myView', ['time'=>$t]);
    }

    // Blade
    public function blade($str) {
        $khoahoc = "<b>Laravel - Khoa Pham</b>";

        if( $str == "laravel" )
            return view('pages.laravel', ['khoahoc'=>$khoahoc]);
        elseif($str == "php")
            return view('pages.php', ['khoahoc'=>$khoahoc]);
    }


}
