<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        
        // Thêm dữ liệu mẫu
        // DB::table('users')->insert([
        //     'name'  =>  'Phu',
        //     'email' =>  'phubui2702@gmail.com',
        //     'password'  =>  bcrypt('matkhau')
        // ]);
        
        $this->call(userSeeder::class);
    }
}

// Tách ra gọi nhiều Seeder cho logic
class userSeeder extends Seeder {
    public function run() {
        DB::table('users')->insert([
            ['name'  =>  'Phu', 'email' =>  '1@gmail.com', 'password'  =>  bcrypt('matkhau')],
            ['name'  =>  'Phu2', 'email2' =>  '2@gmail.com', 'password2'  =>  bcrypt('matkhau')],
            ['name'  =>  'Phu3', 'email3' =>  '3@gmail.com', 'password3'  =>  bcrypt('matkhau')],
        ]);
    }
}