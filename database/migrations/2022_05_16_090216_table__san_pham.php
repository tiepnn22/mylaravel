<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableSanPham extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()    // Sẽ chạy khi thực thi migrate
    {
        //
        Schema::create('sanpham', function ($table) {
            $table->increments('id');
            $table->string('ten');
            $table->integer('soluong');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()  // Sẽ chạy khi rollback lại migrate, quay lại bước trước khi thực hiện function up
    {
        //
        Schema::drop('sanpham');
    }
}
