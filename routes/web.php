<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route
Route::get('/', function (){
    return view('welcome');
});

// Truyền tham số
    Route::get('HoTen/{ten}', function($ten){
        echo 'Tên của bạn là : '.$ten;
    });

// Đặt điều kiện trên tham số
    Route::get('NgaySinh/{ngay}', function($ngay){
        echo 'Ngày sinh của bạn là : '.$ngay;
    })->where(['ngay'=>'[0-9]+']);

// Định danh (để gọi được route trong route)
    //Cách viết 1
    Route::get('Route1', ['as'=>'MyRoute1',function(){
        echo 'Định danh Route1';
    }]);
    //Cách viết 2
    Route::get('Route2', function(){
        echo 'Định danh Route2';
    })->name('MyRoute2');

    Route::get('GoiTen', function(){
        // return redirect()->route('MyRoute1');
        return redirect()->route('MyRoute2');
    });

// Group route (gọi MyGroup/User1, hoặc MyGroup/User2, hoặc MyGroup/User3)
    Route::group(['prefix'=>'MyGroup'],function(){
        Route::get('User1', function () {
            echo 'User1';
        });
        Route::get('User2', function () {
            echo 'User2';
        });
        Route::get('User3', function () {
            echo 'User3';
        });
    });

// Controller
use App\Http\Controllers\MyController;
    // Route::get('GoiController', [MyController::class, 'XinChao']);
    Route::get('Goicontroller' ,'App\Http\Controllers\MyController@Xinchao');

// Truyền tham số cho Controller
    Route::get('GoiControllerTruyenBien/{ten}', 'App\Http\Controllers\MyController@KhoaHoc');

// URL trên Request
    Route::get('MyRequest', 'App\Http\Controllers\MyController@GetURL');

// Gửi nhận dữ liệu trên Request với Form
    Route::get('getForm', function(){
        return view('postForm');
    });

    // Nhận dữ liệu từ form gửi lên, đưa vào Controllers
    // postForm_url : tên đường dẫn
    Route::post('postForm_url', [
        'as'=>'postForm',   // Để gọi Route trong hệ thống thì phải gọi Route qua định danh : postForm
        'uses'=>'App\Http\Controllers\MyController@postForm'
    ]);

// Cookie
    Route::get('setCookie', 'App\Http\Controllers\MyController@setCookie');
    Route::get('getCookie', 'App\Http\Controllers\MyController@getCookie');

// Upload file
    Route::get('uploadFile', function(){
        return view('postFile');
    });

    // Nhận dữ liệu từ form gửi lên, đưa vào Controllers
    // postFile_url : tên đường dẫn
    Route::post('postFile_url', [
        'as'=>'postFile',   // Để gọi Route trong hệ thống thì phải gọi Route qua định danh : postFile
        'uses'=>'App\Http\Controllers\MyController@postFile'
    ]);

// Xuất dữ liệu dạng JSON
    Route::get('getJson', 'App\Http\Controllers\MyController@getJson');

    // View
    // Kết nối sang file View
    Route::get('myView', 'App\Http\Controllers\MyController@myView');
    // Truyền tham số sang View
    Route::get('Time/{t}', 'App\Http\Controllers\MyController@Time');
    // Dùng chung dữ liệu trên View (ở file myView, view/KhoaPham)
    View::share('KhoaHoc', 'Laravel');

// Blade template
    // Route::get('blade', function(){
        // return view('pages.laravel');
        // return view('pages.php');
    // });
    Route::get('BladeTemplate/{str}', 'App\Http\Controllers\MyController@blade');

// Database
    // $table->primary('TenKhoaChinh');     Tạo khoá chính
    // $table->forgein('KhoaPhu')->references('KhoaChinh')->on('Bang');     Tạo khoá phụ
    // $table->unique('TenCot');            Ràng buộc unique
    // $table->time();                      Kiểu giờ
    // $table->datetime();                  Kiểu ngày, giờ
    // $table->date();                      Kiểu ngày
    // $table->text();                      Kiểu text
    // $table->float();                     Kiểu float
    // $table->boolean();                   Kiểu logic
    // $table->rememberToken();             Tạo Token
    // ->nullable();                        Cho phép giá trị null
    // ->default($value);                   Gán giá trị mặc định cho cột
    // ->unsigned();                        Đặt unsigned cho integer

    // Create
    Route::get('database', function() {
        // Schema::create('loaisanpham', function ($table) {
        //     $table->increments('id');
        //     $table->string('ten', 200);
        // });

        Schema::create('theloai', function ($table) {
            $table->increments('id');
            $table->string('ten', 200)->nullable();
            $table->string('nsx')->default('Nha san xuat');
        });

        echo "Đã thực hiện tạo bảng";
    });

    // Liên kết
    Route::get('lienketbang', function() {
        Schema::create('sanpham', function ($table) {
            $table->increments('id');
            $table->string('ten');
            $table->float('gia');
            $table->integer('soluong')->default(0);
            $table->integer('id_loaisanpham')->unsigned();
            $table->foreign('id_loaisanpham')->references('id')->on('loaisanpham');
        });

        echo "Đã tạo bảng sản phẩm";
    });

    // Delete column
    Route::get('suabang', function() {
        Schema::table('theloai', function($table) {
            $table->dropColumn('nsx');
        });
    });
    // Add column
    Route::get('themcot', function() {
        Schema::table('theloai', function($table) {
            $table->string('Email');
        });

        echo "Đã thêm cột Email";
    });
    
    // Rename table
    Route::get('doiten', function() {
        Schema::rename('theloai', 'nguoidung');
        
        echo "Đã đổi tên bảng";
    });
    // Delete table
    Route::get('xoabang', function() {
        // Schema::drop('nguoidung');           // Xoá bảng ngay lập tức, nếu load nhiều -> lỗi vì ko thấy bảng để xoá
        Schema::dropIfExists('nguoidung');      // Xoá bảng nếu bảng tồn tại
        
        echo "Đã xoá bảng";
    });

// Query builder
    // DB::table('users')                                           Chọn bảng trong csdl
    // DB::table('users')->get();                                   Lấy dữ liệu trong bảng
    // DB::table('users')->where('name','Phu')->first();            Lấy dòng dữ liệu đầu tiên
    // DB::table('users')->where('name','Phu2')->value('email');    Trả về dữ liệu theo dk where
    // DB::table('users')->select('name','email')->get();           Chọn tên cột cần truy vấn
    /* $query = DB::table('users')->select('name');                 Thêm cột vào truy vấn trước đó với addSelect()
       $users = $query->addSelect('age')->get();                    */
    
    // DB::table('users')->select(DB::raw('id,name as hoten,email'));                   Thêm lệnh truy vấn vào select()
    // DB::table('users')->join('contacts','users.id','=','contacts.user_id')->select('contacts.phone')->get;   Lệnh join liên kết bảng trong truy vấn
    // DB::table('users')->where('votes','=',100)->get();                               Điều kiện where
    // DB::table('users')->where('votes','=',100)->orwhere('age','>=','18')->get();     Điều kiện hoặc
    // DB::table('users')->orderBy('name','desc')->get();                               Lệnh orderby
    // DB::table('users')->groupBy('account_id')->having('account_id','>',100)->get();  Lệnh groupby
    // DB::table('users')->skip(10)->take(5)->get();                                    Limit : bỏ qua 10 value, lấy 5 value tiếp
    // DB::table('orders')->where('finalized',1)->avg('price');                         Lấy giá trị trung bình
    // DB::table('orders')->max('price');                                               Lấy giá trị max
    // DB::table('users')->count();                                                     Lệnh count
    
    // DB::table('users')->where('id',1)->update(['votes'=>1]);                         Lệnh update
    // DB::table('users')->increment('votes',4);                                        Lệnh tăng/giảm (decrement) giá trị
    // DB::table('users')->insert(['email'=>'john@example.com','votes'=>0]);            Lệnh insert
    // DB::table('users')->where('votes','<',100)->delete();                            Lệnh delete
    // DB::table('users')->truncate();                                                  Xoá all dữ liệu trong bảng và đặt chỉ số tự tăng về 0

    // Get
    Route::get('qb/get', function() {
        $data = DB::table('users')->get();

        foreach ($data as $row) {
            foreach ($row as $key => $value) {
                echo $key.":".$value."<br>";
            }
            echo "<hr>";
        }
    });
    // select * from users where id = 7
    Route::get('qb/where', function() {
        $data = DB::table('users')->where('id','=',7)->get();

        foreach ($data as $row) {
            foreach ($row as $key => $value) {
                echo $key.":".$value."<br>";
            }
            echo "<hr>";
        }
    });
    // select id, name, email ...
    Route::get('qb/select', function() {
        $data = DB::table('users')->select(['id','name','email'])->where('id',7)->get();

        foreach ($data as $row) {
            foreach ($row as $key => $value) {
                echo $key.":".$value."<br>";
            }
            echo "<hr>";
        }
    });
    // select name as hoten form... (đổi trường name thành họ tên)
    Route::get('qb/raw', function() {
        $data = DB::table('users')->select(DB::raw('id,name as hoten,email'))->where('id',7)->get();

        foreach ($data as $row) {
            foreach ($row as $key => $value) {
                echo $key.":".$value."<br>";
            }
            echo "<hr>";
        }
    });
    // orderby
    Route::get('qb/orderby', function() {
        $data = DB::table('users')->select(DB::raw('id,name as hoten,email'))->where('id','>',1)->orderby('id','desc')->get();

        foreach ($data as $row) {
            foreach ($row as $key => $value) {
                echo $key.":".$value."<br>";
            }
            echo "<hr>";
        }
    });
    // update
    Route::get('qb/update', function() {
        DB::table('users')->where('id',1)->update(['name'=>'phu','email'=>'phu@email.com']);
        echo "Đã update";
    });
    // delete
    Route::get('qb/delete', function() {
        DB::table('users')->where('id','=',1)->delete();
        echo "Đã xoá";
    });

// Model
    /* $user = new User();                 // Lấy giá trị thuộc tính của model
    echo $user->name; */
    // $user = App\Models\User::all();     // Lấy toàn bộ dữ liệu trong bảng
    // $user = App\Models\User::find(19);  // Tìm user theo khoá chính
    // $user->toJson();                    // Trả dữ liệu kiểu json
    // $user->save();                      // Lưu dữ liệu vào bảng
    // $user->delete();                    // Xoá dữ liệu trơng bảng
    // User::destroy(19)                   // Xoá dữ liệu bằng khoá chính trơng bảng

    // Create
    Route::get('model/save', function() {
        $user = new App\Models\User();
        $user->name = "Mai";
        $user->email = "Mai@Email.com";
        $user->password = "Mat khau";

        $user->save();
        // $user->delete();    // Xoá

        echo "Đã thực hiện save()";
    });

    // Find
    Route::get('model/query', function() {
        $user = App\Models\User::find(19);

        echo $user->name;
    });

    // Create dữ liệu theo biến
    Route::get('model/sanpham/save/{ten}', function($ten) {
        $user = new App\Models\SanPham();
        $user->ten = $ten;
        $user->soluong = 100;

        $user->save();

        echo "Đã lưu ".$ten;
    });

    // Get all
    Route::get('model/sanpham/all', function() {
        $sanpham = App\Models\SanPham::all()->toJson();
        
        echo $sanpham;
    });

    // Kết hợp query builder
    Route::get('model/sanpham/ten', function() {
        $sanpham = App\Models\SanPham::where('ten','Laptop')->get()->toArray();
        
        var_dump($sanpham);
    });

    // Xoá
    Route::get('model/sanpham/delete', function() {
        App\Models\SanPham::destroy(4);

        echo "Đã xoá";
    });

// Liên kết dữ liệu trong Model
    Route::get('taocot', function() {
        Schema::table('sanpham', function($table) {
            $table->integer('id_loaisanpham')->unsigned();
        });
    });

    // Lấy ra thông tin id_loaisanpham của sanpham có id = 3
    Route::get('lienket', function() {
        $data = App\Models\SanPham::find(3)->loaisanpham->toArray();

        var_dump($data);
    });

    // Lấy ra những sanpham có id_loaisanpham = 1
    Route::get('lienketloaisanpham', function() {
        $data = App\Models\LoaiSanPham::find(1)->sanpham->toArray();

        var_dump($data);
    });

