@extends('layouts.master')

@section('NoiDung')

	{{-- if --}}
	@if( isset($khoahoc) )
		{{ $khoahoc }}
		{!! $khoahoc !!}
	@else
		{{ "Không có khoá học" }}
	@endif
	<br>

	{{-- for --}}
	@for ($i = 1; $i <= 10; $i++)
		{{ $i." " }}	
	@endfor
	<br>

	<?php $khoahoc_array = array('PHP', 'IOS', 'ASP'); ?>
	{{-- foreach --}}
	@foreach ($khoahoc_array as $value)
		{{ $value }}
	@endforeach
	<br>

	{{-- forelse --}}
	@forelse ($khoahoc_array as $value)
		{{-- @continue( $value == "IOS" )	Nếu tìm thấy, sẽ bỏ qua giá trị --}}
		{{-- @break( $value == "IOS" )		Nếu tìm thấy, sẽ thoát vòng for ngay lập tức --}}
		{{ $value }}
	@empty
		{{ "Mảng rỗng" }}
	@endforelse

@endsection